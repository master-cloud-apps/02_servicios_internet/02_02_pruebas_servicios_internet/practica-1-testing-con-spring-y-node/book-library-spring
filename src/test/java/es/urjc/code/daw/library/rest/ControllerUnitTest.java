package es.urjc.code.daw.library.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import es.urjc.code.daw.library.book.Book;
import es.urjc.code.daw.library.book.BookFakeRepository;
import es.urjc.code.daw.library.book.BookRepository;
import es.urjc.code.daw.library.book.TestPreparation;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

@SpringBootTest
@AutoConfigureMockMvc
public abstract class ControllerUnitTest {
    @Autowired
    protected MockMvc mockMvc;

    @Autowired
    protected ObjectMapper objectMapper;

    @MockBean
    protected BookRepository bookRepository;
    protected BookFakeRepository bookFakeRepository = new BookFakeRepository();

    private TestPreparation testPreparation;

    public ControllerUnitTest() {
        this.testPreparation = new TestPreparation();
    }

    @BeforeEach
    void setUp() {
        this.testPreparation.prepareMocks(bookRepository, bookFakeRepository);
    }

    public Book createBook() throws Exception {
        Book newBook = new Book("Cien años de Soledad", "Libro de Gabo");
        MvcResult result = this.mockMvc.perform(
                post("/api/books/")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(this.objectMapper.writeValueAsString(newBook)))
                .andReturn();

        return objectMapper.readValue(result.getResponse().getContentAsString(), Book.class);
    }
}
