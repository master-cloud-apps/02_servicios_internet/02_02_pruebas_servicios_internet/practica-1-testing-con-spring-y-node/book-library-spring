package es.urjc.code.daw.library.rest;


import es.urjc.code.daw.library.book.Book;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;

import static es.urjc.code.daw.library.book.TestConstants.API_BOOK_URL;
import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

class BookControllerAnonymousTest extends ControllerUnitTest {

    @Test
    void givenNotLoggedUser_whenGetAllBooks_shouldReturnDefaultBooks() throws Exception {
        this.mockMvc.perform(get(API_BOOK_URL))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(5)));
    }

    @Test
    void givenNotLoggedUser_whenCreateBook_shouldUnauthenticated() throws Exception {
        Book newBook = new Book("Cien años de Soledad", "Libro de Gabo");

        this.mockMvc.perform(
                post("/api/books/")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(this.objectMapper.writeValueAsString(newBook)))
                .andExpect(status().isUnauthorized());
    }
}
