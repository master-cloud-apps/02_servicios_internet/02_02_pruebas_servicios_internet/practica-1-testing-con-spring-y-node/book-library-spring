package es.urjc.code.daw.library.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import es.urjc.code.daw.library.book.Book;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MvcResult;

import static es.urjc.code.daw.library.book.TestConstants.*;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class BookControllerUserTest extends ControllerUnitTest {

    @Autowired
    ObjectMapper objectMapper;

    @Test
    @WithMockUser(username = USER_NAME, password = USER_PASS, roles = USER_ROLE)
    void givenUserWithRoleUserLogged_whenCreateBook_shouldReturnOK() throws Exception {
        Book newBook = new Book("Cien años de Soledad", "Libro de Gabo");

        MvcResult result = this.mockMvc.perform(
                post("/api/books/")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(this.objectMapper.writeValueAsString(newBook)))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.title").value("Cien años de Soledad"))
                .andReturn();

        Book response = objectMapper.readValue(result.getResponse().getContentAsString(), Book.class);
        assertEquals(response.getDescription(), "Libro de Gabo");
        this.bookRepository.deleteById(response.getId());
    }

    @Test
    @WithMockUser(username = USER_NAME, password = USER_PASS, roles = USER_ROLE)
    void givenUserWithRoleUserLogged_whenGetAllBooks_shouldReturnDefaultBooks() throws Exception {
        this.mockMvc.perform(get(API_BOOK_URL))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(5)));
    }

    @Test
    @WithMockUser(username = USER_NAME, password = USER_PASS, roles = USER_ROLE)
    void givenUserWithRoleUserLogged_whenDeleteExistingBook_shouldReturnUnAuthorized() throws Exception {
        Book response = this.createBook();

        this.mockMvc.perform(delete(API_BOOK_URL + response.getId()))
                .andExpect(status().isForbidden());
    }

}
