package es.urjc.code.daw.library.rest.assured;

import es.urjc.code.daw.library.book.Book;
import io.restassured.RestAssured;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static es.urjc.code.daw.library.book.TestConstants.*;
import static io.restassured.RestAssured.given;

class BookControllerAdminRoleIT extends RestAssuredConfigurationIT {


    @Test
    void givenUserWithRoleAdminLoggedAndBookCreated_whenDeleteBook_shouldReturnOK() {
        Book result = this.createBook(ADMIN_NAME, ADMIN_PASS);

        this.setUp();
        given().auth().basic(ADMIN_NAME, ADMIN_PASS)
                .when().get(API_BOOK_URL + result.getId())
                .then().statusCode(200);

        this.setUp();
        given().auth().basic(ADMIN_NAME, ADMIN_PASS)
                .when().delete(API_BOOK_URL + result.getId())
                .then().statusCode(200);

        this.setUp();
        given().auth().basic(ADMIN_NAME, ADMIN_PASS)
                .when().get(API_BOOK_URL + result.getId())
                .then().statusCode(404);
    }


}
