package es.urjc.code.daw.library.rest.wtc;

import es.urjc.code.daw.library.book.Book;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;

import static es.urjc.code.daw.library.book.TestConstants.*;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.web.reactive.function.client.ExchangeFilterFunctions.basicAuthentication;

public class BookControllerWTCIT extends WTCIT {

    private static final Book TEST_NEW_BOOK = new Book("El libro del testing", "Junit y mas cosas");

    @Test
    void givenNoRoleUser_whenGetBooks_shouldReturnAllBooks() {
        this.wtc.get().uri(API_BOOK_URL).exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$").value(hasSize(5));
    }

    @Test
    void givenNoRoleUser_whenDeleteBook_shouldReturnNotAllowed() {
        this.wtc.delete().uri(API_BOOK_URL + 2).exchange()
                .expectStatus().isUnauthorized();
    }

    @Test
    void givenLoggedUser_whenDeleteBook_shouldReturnNotAllowed() {
        this.wtc.mutate().filter(basicAuthentication(USER_NAME, USER_PASS)).build()
                .delete().uri(API_BOOK_URL + 2).exchange()
                .expectStatus().isForbidden();
    }

    @Test
    void givenLoggedUser_whenCreateBook_shouldReturnOK() {
        Book newBook = this.createBook();

        assertEquals(TEST_NEW_BOOK.getDescription(), newBook.getDescription());

        this.deleteBookById(newBook.getId());
    }


    @Test
    void givenAdminUser_whenDeleteBook_shouldReturnOKandNoBookFound() {
        Book newBook = this.createBook();

        this.wtc.mutate().filter(basicAuthentication(ADMIN_NAME, ADMIN_PASS)).build()
                .delete().uri(API_BOOK_URL + newBook.getId()).exchange()
                .expectStatus().isOk();

        this.wtc.mutate().filter(basicAuthentication(USER_NAME, USER_PASS)).build()
                .get().uri(API_BOOK_URL + newBook.getId()).exchange()
                .expectStatus().isNotFound();
    }

    private void deleteBookById(Long bookId) {
        this.wtc.mutate().filter(basicAuthentication(ADMIN_NAME, ADMIN_PASS)).build().
                delete().uri(API_BOOK_URL + bookId).exchange();
    }

    private Book createBook() {
        return this.wtc.mutate().filter(basicAuthentication(USER_NAME, USER_PASS)).build().post()
                .uri(API_BOOK_URL)
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue(TEST_NEW_BOOK)
                .exchange()
                .expectStatus().isCreated()
                .expectBody(Book.class)
                .returnResult()
                .getResponseBody();
    }
}
